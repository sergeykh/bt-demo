package com.fidesmo.bt_demo;

import android.annotation.TargetApi;
import android.app.Service;
import android.bluetooth.*;
import android.bluetooth.le.AdvertiseCallback;
import android.bluetooth.le.AdvertiseData;
import android.bluetooth.le.AdvertiseSettings;
import android.bluetooth.le.BluetoothLeAdvertiser;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.IBinder;
import android.os.ParcelUuid;
import android.widget.Toast;

import java.util.UUID;

/**
 * Created by sergeykhruschak on 6/7/16.
 */
@TargetApi(21)
public class BlePeripheralService extends Service {

    private final IBinder binder = new LocalBinder();

    private BluetoothManager btManager;
    private BluetoothAdapter btAdapter;
    private BluetoothGattServerCallback gattServerCallback;
    private BluetoothGattServer gattServer;

    private AdvertiseSettings advertSettings;
    private AdvertiseCallback advertCallback;

    public BlePeripheralService() {

        advertSettings = new AdvertiseSettings.Builder()
                .setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_BALANCED)
                .setTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_MEDIUM)
                .setConnectable(true)
                .setTimeout(0)
                .build();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    @Override
    public void onCreate() {
        close();

        btManager = (BluetoothManager) getSystemService(BLUETOOTH_SERVICE);
        btAdapter = btManager.getAdapter();
        startServer();
        startAdvertisement();
    }

    public void onDestroy() {
        close();
    }

    public void startAdvertisement() {
        log("Starting to advertise device");

        if (!btAdapter.isMultipleAdvertisementSupported()) {
            log("No Advertising Support");
            return;
        }

        ParcelUuid pUuid = new ParcelUuid(UUID.fromString(getString(R.string.ble_uuid)));

        AdvertiseData data = new AdvertiseData.Builder()
                .setIncludeDeviceName(false)
                .addServiceUuid(pUuid)
                .build();

        BluetoothLeAdvertiser advertiser = btAdapter.getBluetoothLeAdvertiser();

        if (advertiser == null) {
            log("Advertising is not supported on this device");
            Toast.makeText(this, "Advertising is not supported", Toast.LENGTH_LONG).show();
            return ;
        }

        if (advertCallback == null) {
            advertCallback = new AdvertiseCallback() {
                @Override
                public void onStartSuccess(AdvertiseSettings settingsInEffect) {
                    super.onStartSuccess(settingsInEffect);
                    log("Advertising started: " + settingsInEffect + ", device: " + btAdapter.getName() + ", addr: " + btAdapter.getAddress());
                }

                @Override
                public void onStartFailure(int errorCode) {
                    super.onStartFailure(errorCode);

                    String errCause = "unknown";

                    switch(errorCode) {
                        case AdvertiseCallback.ADVERTISE_FAILED_ALREADY_STARTED:
                            errCause = "Already started";
                            break;
                        case AdvertiseCallback.ADVERTISE_FAILED_DATA_TOO_LARGE:
                            errCause = "Data too large";
                            break;
                        case AdvertiseCallback.ADVERTISE_FAILED_FEATURE_UNSUPPORTED:
                            errCause = "Feature unsupported";
                            break;
                        case AdvertiseCallback.ADVERTISE_FAILED_INTERNAL_ERROR:
                            errCause = "Internal error";
                            break;
                        case AdvertiseCallback.ADVERTISE_FAILED_TOO_MANY_ADVERTISERS:
                            errCause = "Too many advertisers";
                            break;

                    }

                    log("Advertising onStartFailure: " + errCause + "(" + errorCode + ")");

                    close();
                }
            };
        }


        advertiser.startAdvertising(advertSettings, data, advertCallback);
    }

    public void startServer() {
        gattServerCallback = new BluetoothGattServerCallback() {
            @Override
            public void onConnectionStateChange(BluetoothDevice device, int status, int newState) {
                log("onConnectionStateChange");
            }

            @Override
            public void onServiceAdded(int status, BluetoothGattService service) {
                log("serviceAdded: " + service.getUuid() + ", adding characteristics");
            }

            @Override
            public void onCharacteristicReadRequest(BluetoothDevice device, int requestId, int offset, BluetoothGattCharacteristic characteristic) {
                super.onCharacteristicReadRequest(device, requestId, offset, characteristic);
                log("onCharacteristicReadRequest: " + characteristic.getUuid());

                byte[] fullValue = getString(R.string.test_data).getBytes();

                //check
                if (offset > fullValue.length) {
                    gattServer.sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, 0, new byte[]{0} );
                    return;
                }

                int size = fullValue.length - offset;
                byte[] response = new byte[size];

                for (int i = offset; i < fullValue.length; i++) {
                    response[i - offset] = fullValue[i];
                }

                if (!characteristic.getUuid().equals(UUID.fromString(getString(R.string.read_characteristics_uuid)))) {
                    gattServer.sendResponse(device, requestId, BluetoothGatt.GATT_FAILURE, 0, null);
                    return ;
                }

                gattServer.sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, offset, response);
            }

            @Override
            public void onCharacteristicWriteRequest(BluetoothDevice device, int requestId, BluetoothGattCharacteristic characteristic, boolean preparedWrite, boolean responseNeeded, int offset, byte[] value) {
                log("onCharacteristicWriteRequest: " + characteristic.getUuid() + ", value: " + new String(value));
            }

            @Override
            public void onDescriptorReadRequest(BluetoothDevice device, int requestId, int offset, BluetoothGattDescriptor descriptor) {
                super.onDescriptorReadRequest(device, requestId, offset, descriptor);
                log("onDescriptorReadRequest: " + descriptor.getUuid() + " char: " + descriptor.getCharacteristic().getUuid());
            }

            @Override
            public void onDescriptorWriteRequest(BluetoothDevice device, int requestId, BluetoothGattDescriptor descriptor, boolean preparedWrite, boolean responseNeeded, int offset, byte[] value) {
                log("onDescriptorWriteRequest: " + descriptor.getUuid() + " char: " + descriptor.getCharacteristic().getUuid());
            }

            @Override
            public void onExecuteWrite(BluetoothDevice device, int requestId, boolean execute) {
                log("onExecuteWrite");
            }

            @Override
            public void onNotificationSent(BluetoothDevice device, int status) {
                log("onNotificationSent");
            }

            @Override
            public void onMtuChanged(BluetoothDevice device, int mtu) {
                log("onMtuChanged");
            }
        };

        log("Starting Gatt Server");

        if(gattServer == null) {

            gattServer = btManager.openGattServer(this, gattServerCallback);

            BluetoothGattService service = new BluetoothGattService(UUID.fromString(getString(R.string.ble_uuid)),
                                                                        BluetoothGattService.SERVICE_TYPE_PRIMARY);

            BluetoothGattCharacteristic readCharacteristic =
                    new BluetoothGattCharacteristic(UUID.fromString(getString(R.string.read_characteristics_uuid)),
                            BluetoothGattCharacteristic.FORMAT_UINT8 | BluetoothGattCharacteristic.PROPERTY_READ,
                            BluetoothGattCharacteristic.PERMISSION_READ );

            BluetoothGattCharacteristic writeCharacteristic =
                    new BluetoothGattCharacteristic(UUID.fromString(getString(R.string.write_characteristics_uuid)),
                            BluetoothGattCharacteristic.FORMAT_UINT8 | BluetoothGattCharacteristic.PROPERTY_WRITE,
                            BluetoothGattCharacteristic.PERMISSION_WRITE);

            service.addCharacteristic(readCharacteristic);
            service.addCharacteristic(writeCharacteristic);

            boolean result = gattServer.addService(service);
            log("Added custom service: " + result);

            for(BluetoothGattService s : gattServer.getServices()) {
                log("Registered services: " + s.getUuid());
            }
        }
    }

    private void log(String s) {
        SharedPreferences preferences = getSharedPreferences("logs", MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("log", s);
        editor.apply();
    }

    public void close() {
        if (advertCallback != null) {
            btAdapter.getBluetoothLeAdvertiser().stopAdvertising(advertCallback);
            log("Advertising stopped");
            advertCallback = null;
        }

        if(gattServer != null) {
            gattServer.close();
        }
    }

    public class LocalBinder extends Binder {}
}
