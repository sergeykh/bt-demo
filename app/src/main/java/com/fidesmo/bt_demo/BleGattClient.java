package com.fidesmo.bt_demo;

import android.annotation.TargetApi;
import android.bluetooth.*;
import android.os.Build;

import java.io.Closeable;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * Created by sergeykhruschak on 6/7/16.
 */
@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
public class BleGattClient implements Closeable {

    private BluetoothGatt gatt;
    private MainActivity context;

    public BleGattClient(MainActivity context) {
        this.context = context;
    }

    public void connect(BluetoothDevice device) {
        gatt = device.connectGatt(context, false, gattClientCallback);
    }

    public void close() {
        if (gatt != null) {
            gatt.close();
        }
    }

    private void printServicesInfo(List<BluetoothGattService> services) {
        for (BluetoothGattService srvc: services ) {
            log("Service[id=" + srvc.getInstanceId() + ", type=" + srvc.getType() + ",uuid=" + srvc.getUuid() + "]");
            List<BluetoothGattCharacteristic> characteristics = srvc.getCharacteristics();

            for (BluetoothGattCharacteristic ch : characteristics) {
                log("characteristics[uuid=" + ch.getUuid() +
                        ", instanceId=" + ch.getInstanceId() + ", value=" + Arrays.toString(ch.getValue()) +
                        ", props=" + ch.getProperties() + ", perms=" + ch.getPermissions() + ", writeType=" + ch.getWriteType() + "]");

                for (BluetoothGattDescriptor descr : ch.getDescriptors()) {
                    log("char_descr[uuid=" + descr.getUuid() + ", value=" + Arrays.toString(descr.getValue()) + ", permissions=" + descr.getPermissions() + "]");
                }
            }

            printServicesInfo(srvc.getIncludedServices());
        }
    }

    private void log(String s) {
        context.log(s);
    }

    private UUID asUuid(int resId) {
        return UUID.fromString(context.getString(resId));
    }

    private final BluetoothGattCallback gattClientCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            log("Connection state: " + status);

            switch (newState) {
                case BluetoothProfile.STATE_CONNECTED:
                    log("STATE_CONNECTED");
                    log("Discovering services");
                    gatt.discoverServices();
                    break;
                case BluetoothProfile.STATE_DISCONNECTED:
                    log("STATE_DISCONNECTED");
                    break;
                default:
                    log("STATE_OTHER");
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            List<BluetoothGattService> services = gatt.getServices();
            log("Discovered " + services.size() + " services");

            printServicesInfo(services);

            BluetoothGattService customService = gatt.getService(UUID.fromString(context.getString(R.string.ble_uuid)));

            if (customService != null) {

                BluetoothGattCharacteristic readChar = customService.getCharacteristic(asUuid(R.string.read_characteristics_uuid));

                boolean initiated = gatt.readCharacteristic(readChar);

                log("Reading custom service[" + customService.getUuid() + "] characteristic: " + readChar.getUuid() + ", init: " + initiated);
            } else {
                log("Custom service not found!");
                gatt.disconnect();
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            log("Characteristics[" + characteristic.getUuid() + "] read: " + (characteristic.getValue() != null ? new String(characteristic.getValue()) : "null"));

            // on read complete write another characteristics to the server
            BluetoothGattService customService = gatt.getService(asUuid(R.string.ble_uuid));
            BluetoothGattCharacteristic writeChar = customService.getCharacteristic(asUuid(R.string.write_characteristics_uuid));

            writeChar.setValue(context.getString(R.string.test_data));

            boolean initiated = gatt.writeCharacteristic(writeChar);
            log("Writing custom service[" + customService.getUuid() + "] characteristic: " + writeChar.getUuid() + ", init: " + initiated);
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            log("Characteristics[" + characteristic.getUuid() + "] write: " + (characteristic.getValue() != null ? new String(characteristic.getValue()) : "null"));
        }
    };
}
