package com.fidesmo.bt_demo;

import android.Manifest;
import android.annotation.TargetApi;
import android.bluetooth.*;
import android.bluetooth.le.*;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.IBinder;
import android.os.ParcelUuid;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static android.view.View.VISIBLE;

@TargetApi(23)
public class MainActivity extends AppCompatActivity {
    private String TAG = getClass().getSimpleName();

    final private int REQUEST_CODE_SCAN = 123;
    final private int REQUEST_CODE_ADVERT = 124;

    private BluetoothAdapter btAdapter;
    private ScanCallback scanCallback;

    private BluetoothManager btManager;
    private BleGattClient gattClient;

    private ServiceConnection bleServiceConnection;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btManager = (BluetoothManager) getSystemService(BLUETOOTH_SERVICE);
        btAdapter = btManager.getAdapter();

        // Use this check to determine whether BLE is supported on the device. Then you can selectively disable BLE-related features.
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, "BLE is not supported on this device", Toast.LENGTH_SHORT).show();
            finish();
        }

        // shared object used for logging from service
        SharedPreferences preferences = getSharedPreferences("logs", MODE_PRIVATE);
        preferences.registerOnSharedPreferenceChangeListener(new SharedPreferences.OnSharedPreferenceChangeListener() {
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
                if("log".equals(key)) {
                    log(sharedPreferences.getString(key, ""));
                }
            }
        });

        askForBtDevicePermissionsAndFireAction(REQUEST_CODE_ADVERT);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(bleServiceConnection);
        onDiscoveryStopClicked(null);

        if (gattClient != null) {
            gattClient.close();
        }
    }

    public void onDiscoveryClicked(View v) {
        clearLog();
        log("Discovery clicked");
        enableScanButton(false);
        askForBtDevicePermissionsAndFireAction(REQUEST_CODE_SCAN);
    }

    private void askForBtDevicePermissionsAndFireAction(int requestCode) {
        int hasWriteBtPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH_ADMIN);
        int hasWriteLocPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);

        if (hasWriteBtPermission != PackageManager.PERMISSION_GRANTED || hasWriteLocPermission != PackageManager.PERMISSION_GRANTED ) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.BLUETOOTH_ADMIN)) {
                Toast.makeText(MainActivity.this, "App needs bluetooth to work", Toast.LENGTH_SHORT).show();
                enableScanButton(false);
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[] {Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.ACCESS_COARSE_LOCATION},
                        requestCode);

                log("Requesting permissions");
            }

            return ;
        }

        enableBluetoothAndAction(REQUEST_CODE_ADVERT);
    }

    public void onDiscoveryStopClicked(View v) {
        if (scanCallback != null) {
            log("Discovery Stop clicked");
            btAdapter.getBluetoothLeScanner().stopScan(scanCallback);
            scanCallback = null;
        }

        enableScanButton(true);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_SCAN:
            case REQUEST_CODE_ADVERT:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                    grantResults[1] == PackageManager.PERMISSION_GRANTED &&
                    grantResults[2] == PackageManager.PERMISSION_GRANTED) {

                    enableBluetoothAndAction(requestCode);
                } else {
                    Toast.makeText(MainActivity.this, "Permission Denied", Toast.LENGTH_SHORT).show();
                }

                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void enableBluetoothAndAction(int requestCode) {
        if (btAdapter == null || !btAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, requestCode);
            enableScanButton(true);
            return;
        }

        log("Bluetooth is enabled. Moving to Scan/Advert");

        onActivityResult(requestCode, 0, null);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE_ADVERT:
                startAdvertise();
                break;
            case REQUEST_CODE_SCAN:
                startScan();
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void startScan() {
        BluetoothLeScanner scanner = btAdapter.getBluetoothLeScanner();

        log("Starting scan...");

        if(scanCallback == null) {
            scanCallback = new ScanCallback() {
                @Override
                public void onScanResult(int callbackType, ScanResult result) {
                    log("Scan callback type: " + callbackType + ", result: " + result);

                    BluetoothDevice btDevice = result.getDevice();
                    connectToDevice(btDevice);
                }

                @Override
                public void onBatchScanResults(List<ScanResult> results) {
                    log("Batch Scan result: " + results);
                }

                @Override
                public void onScanFailed(int errorCode) {
                    log("Scan failed: " + errorCode);
                }
            };
        }

        ScanFilter filter = new ScanFilter.Builder()
                .setServiceUuid(new ParcelUuid(UUID.fromString(getString(R.string.ble_uuid))))
                .build();

        List<ScanFilter> filters = new ArrayList<ScanFilter>();
        filters.add(filter);

        ScanSettings settings = new ScanSettings.Builder().setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY).build();

        scanner.startScan(filters, settings, scanCallback);
    }

    private void startAdvertise() {
        Intent intent = new Intent(this, BlePeripheralService.class);

        bleServiceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                log("Advertising service connected");
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                log("Advertising service disconnected");
            }
        };

        bindService(intent, bleServiceConnection, BIND_AUTO_CREATE);
    }

    public void connectToDevice(BluetoothDevice device) {
        log("Connecting to device: " + device);

        gattClient = new BleGattClient(this);
        gattClient.connect(device);

        onDiscoveryStopClicked(null);
    }

    private void clearLog() {
        ((TextView)findViewById(R.id.outputView)).setText("");
    }

    public void log(final String message) {
        runOnUiThread(new Runnable() {
            public void run() {
                TextView tv = (TextView) findViewById(R.id.outputView);
                tv.append(message + "\n");
                Log.i(TAG, message);
            }
        });
    }

    private void enableScanButton(boolean enable) {
        findViewById(R.id.btnScan).setEnabled(enable);
        findViewById(R.id.btnStop).setEnabled(!enable);
        findViewById(R.id.progressBar).setVisibility(enable ? View.INVISIBLE : VISIBLE);
    }
}
